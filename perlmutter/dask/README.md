# Perlmutter

## Using Dask on Perlmutter

Here is a general overview of our suggest best practices
for getting the most out of Dask on Perlmutter.

Questions? Feedback? Please let us know at `help.nersc.gov`.

## What you'll find in this repo

1. Several example notebooks that demonstrate ways to start CPU and GPU Dask clusters
1. Several example scripts that help launch CPU and GPU Dask clusters
1. A sample kernel.json file to help transform a container into a Jupyter kernel

## Suggested Dask Workflow

Jupyter is the de-facto standard for using Python interactively,
and that also applies to Dask. Most of our examples will show you how
to use Dask from within Jupyter. One of the major advantages of this is
that it gives you access to the very useful Dask Dashboard in real-time,
which can give you a clear picture of how your application is using
resources. If you have questions about getting started with Jupyter at
NERSC, please see our
[NERSC Jupyter page](https://docs.nersc.gov/services/jupyter/).

All of our examples will follow the pattern of:

1. Requesting a job via Slurm
1. Spinning up a Dask cluster on Perlmutter compute nodes
1. Starting a Jupyter node on a `Shared CPU node` (i.e. not a compute node)
1. Connecting the notebook to the cluster via the Dask client

This way, Jupyter notebooks can be long-lived relative to the
cluster. If desired, a new cluster can be started and connected to the
existing Jupyter notebook.

## Which configuration should I choose?

We provide 3 examples to show how you can configure and
run Dask with 1) the NERSC Python module, 2) with
a custom conda environment, and 3) with a Shifter 
container.

### Want to go for a quick test drive?

Try 
`01-dask-cpu-module-example.ipynb` to get a sense
of how Dask works. This example uses
Dask packages already installed in the NERSC
Python module.

### Are you already using
[custom conda environments](https://docs.nersc.gov/development/languages/python/nersc-python/#option-2-module-conda-activate)?

Or do you want an easy way to create an environment
with custom libraries to use with Dask? Try 
`02-dask-cpu-conda-example.ipynb` to see
how to use Dask with a custom environment.
We'll also demonstrate how to create and use custom
[Jupyter kernels](https://docs.nersc.gov/services/jupyter/#conda-environments-as-kernels).

### Are you planning to run at large scale?

[Shifter containers](https://docs.nersc.gov/development/shifter/how-to-use/)
can help speed up package imports and improve
runtime, especially at large scale. Try 
`03-dask-gpu-container-example.ipynb` to see
how to use Dask with a container.

### Prefer to run Dask inside a batch job?

If you just want to submit your Dask job to the queue, we
provide an example `submit-dask-batch.slurm`. Check this out
to see how to submit Dask work non-interactively to Slurm. 
This example will submit a Dask job that runs the
`dask-calculate-pi.py` script, which contains an example
that is very similar to the Jupyter example notebooks.
If you run this example, you should see the following
printed in your `slurm-<jobid>.out` output file:

```shell
value of pi is
3.1415968
calculated pi in 21.008206844329834 seconds
```

It will be embedded in the middle of the various Dask cluster
and worker messages.
